package excel

import (
	"fmt"
	"github.com/tealeg/xlsx"
	".././config"
	"strconv"
)

func WriteFile(excelFileName string, generators []Generator, conf config.Config) {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()

	for sheetNo:= 0; sheetNo < conf.SheetCount; sheetNo++ {
		sheet, err = file.AddSheet("Sheet_"+ strconv.Itoa(sheetNo))
		if err != nil {
			fmt.Printf(err.Error())
		}
		addHeader(sheet, generators)
		for rowNo := 0; rowNo < conf.IterationCount; rowNo++ {
			row = sheet.AddRow()
			for _, gen := range generators {
				cell = row.AddCell()
				gen.Generate()
				gen.SetCellValue(cell)
			}
		}
	}

	err = file.Save(excelFileName)
	if err != nil {
		fmt.Printf(err.Error())
	}
}


func addHeader(sheet *xlsx.Sheet, generators []Generator) {
	row := sheet.AddRow()
	for _, gen := range generators {
		cell := row.AddCell()
		cell.Value = gen.FieldName()
	}
}
