package excel

import (
	"../config"
)

func GenerateData(fieldDefinitions []config.FieldDefinition) []Generator {
	var rowInfos []Generator
	for _, def := range fieldDefinitions {
		switch def.Type {
		case config.STRING:
			rowInfo := &StringGenerator{}
			rowInfo.FieldDefinition = def
			rowInfos = append(rowInfos, rowInfo)
		case config.FLOAT:
			rowInfo := &FloatGenerator{}
			rowInfo.FieldDefinition = def
			rowInfos = append(rowInfos, rowInfo)
		case config.DATE:
			rowInfo := &DateGenerator{}
			rowInfo.FieldDefinition = def
			rowInfos = append(rowInfos, rowInfo)
		case config.INTEGER:
			rowInfo := &IntegerGenerator{}
			rowInfo.FieldDefinition = def
			rowInfos = append(rowInfos, rowInfo)
		case config.RANGE:
		case config.NUMBER_RANGE:
		case config.AUTO_NUMBER:
		case config.SEQUENCE:
		}
	}
	return rowInfos
}
