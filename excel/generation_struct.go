package excel

import (
	"../config"
	"github.com/Songmu/strrand"
	"time"
	"math/rand"
	"strings"
	"github.com/tealeg/xlsx"
)

type Generator interface {
	Generate()
	SetCellValue(cell *xlsx.Cell)
	FieldName() string
}

type RowCellInfo struct {
	config.FieldDefinition
}

func (i* RowCellInfo) FieldName() string {
	return i.Name
}


// ================================================

// String
type StringGenerator struct {
	RowCellInfo
	Value string
}

func (g* StringGenerator) Generate() {
	g.Value, _ = strrand.RandomString(g.Pattern)
}

func (g* StringGenerator) SetCellValue(cell *xlsx.Cell) {
	cell.Value = g.Value
}


// Float
type FloatGenerator struct {
	RowCellInfo
	Value float64
}

func (g* FloatGenerator) Generate() {
	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)
	g.Value = random.Float64()
}

func (g* FloatGenerator) SetCellValue(cell *xlsx.Cell) {
	cell.SetFloat(g.Value)
}

// Date
type DateGenerator struct {
	RowCellInfo
	Value time.Time
}

func (g* DateGenerator) Generate() {
	if g.Pattern != "" {
		stringPatternAndDate := strings.Split(g.Pattern, ":")
		if len(stringPatternAndDate) > 1 {
			g.Value, _ = time.Parse(g.reformatDateFormat(stringPatternAndDate[0]), stringPatternAndDate[1])
		} else {
			g.Value = time.Now()
		}
	} else {
		g.Value = time.Now()
	}
}

func (g* DateGenerator) SetCellValue(cell *xlsx.Cell) {
	cell.SetDateTime(g.Value)
}

func (g* DateGenerator) reformatDateFormat(classicDateFormat string) string {
	classicDateFormat = strings.Replace(classicDateFormat, "MM", "01", -1)
	classicDateFormat = strings.Replace(classicDateFormat, "dd", "02", -1)
	classicDateFormat = strings.Replace(classicDateFormat, "yyyy", "2006", -1)

	return classicDateFormat
}


// Integer
type IntegerGenerator struct {
	RowCellInfo
	Value int64
}

func (g* IntegerGenerator) Generate() {
	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)
	g.Value = random.Int63()
}

func (g* IntegerGenerator) SetCellValue(cell *xlsx.Cell) {
	cell.SetInt64(g.Value)
}