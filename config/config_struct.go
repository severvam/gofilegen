package config

type FieldType string

const (
	STRING FieldType = "String"
	FLOAT FieldType = "Float"
	DATE FieldType = "Date"
	INTEGER FieldType = "Integer"
	RANGE FieldType = "Range"
	NUMBER_RANGE FieldType = "NumberRange"
	AUTO_NUMBER FieldType = "AutoNumber"
	SEQUENCE FieldType = "Sequence"
)

type Config struct {
	IterationCount int
	LineSeparator string
	OutputFileName string
	SheetCount int
	FieldDefinitions []FieldDefinition
}

type FieldDefinition struct {
	Name string
	Type FieldType
	Pattern string
}