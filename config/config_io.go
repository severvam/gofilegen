package config

import (
	"github.com/tealeg/xlsx"
	"fmt"
)

func ReadFile(excelFileName string) Config {
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Errorf("error!", err)
	}
	var rowCounter int = 0
	var config Config
	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			if rowCounter < 4 {
				switch rowCounter {
				case 0:
					readHeader(row, func(cell *xlsx.Cell) {
						config.IterationCount, _ = cell.Int()
					})

				case 1:
					readHeader(row, func(cell *xlsx.Cell) {
						config.LineSeparator, _ = cell.String()
					})
				case 2:
					readHeader(row, func(cell *xlsx.Cell) {
						config.OutputFileName, _ = cell.String()
					})
				case 3:
					readHeader(row, func(cell *xlsx.Cell) {
						config.SheetCount, _ = cell.Int()
					})
				}
			} else {
				cellCounter := 0
				var fieldDefinition FieldDefinition
				for _, cell := range row.Cells {
					switch cellCounter {
					case 0:
						fieldDefinition.Name, _ = cell.String()
					case 1:
						temp, _ := cell.String()
						fieldDefinition.Type = FieldType(temp)
					case 3:
						fieldDefinition.Pattern, _ = cell.String()
					}
					cellCounter++
				}
				config.FieldDefinitions = append(config.FieldDefinitions, fieldDefinition)
			}
			rowCounter++
		}
	}
	return config
}

type headerField func(cell *xlsx.Cell)

func readHeader(row *xlsx.Row, fn headerField) {
	cellCounter := 0
	for _, cell := range row.Cells {
		switch cellCounter {
		case 1:
			fn(cell)
		}
		cellCounter++
	}
}