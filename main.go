package main

import (
	"./config"
	"./excel"
)

func main() {
	genConf := config.ReadFile("InputSample2.xlsx")
	data := excel.GenerateData(genConf.FieldDefinitions)
	excel.WriteFile("MyXLSXFile.xlsx", data, genConf)
}
